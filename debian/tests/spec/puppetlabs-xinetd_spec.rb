require 'spec_helper'

describe "xinetd is not installed" do
  describe package('xinetd') do
    it { should_not be_installed }
  end
  describe service('xinetd') do
    it { should_not be_running }
  end
end

describe "test manifest applies successfully" do
  describe command("puppet apply --logdest /var/log/puppet-apply.log --test spec/fixtures/test.pp") do
    its(:exit_status) { should eq 2 }
  end
end

describe "xinetd is installed" do
  describe package('xinetd') do
    it { should be_installed }
  end
end

describe "xinetd service is started" do
  # allow time for the service to start
  before(:all) do
    sleep 5
  end
  describe service('xinetd') do
    it { should be_enabled }
    it { should be_running }
  end
end

describe "test service port is open" do
  describe port(31337) do
    it { should be_listening }
  end
end
