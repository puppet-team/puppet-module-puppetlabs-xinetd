include xinetd

xinetd::service { 'false':
  port         => '31337',
  server       => '/bin/false',
  service_type => 'UNLISTED',
}
